package jp.alhinc.nonoshita_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	//インプットディレクトリのパスをコマンドライン引数から受け取る
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//読み込み用変数
		BufferedReader br = null;

		//支店定義ファイル用Map
		Map<String,String> branchNames = new HashMap<>();

		//商品定義ファイル用Map
		HashMap<String, String> commodityName  = new HashMap<String, String>();

		//支店別売上金額累計用マップ
		Map<String,Long> branchSales = new HashMap<>();

		//商品別売上金額累計用マップ
		HashMap<String, Long> commoditySales  = new HashMap<String, Long>();

		//１．支店定義ファイル読み込み
		//コマンドライン引数の確認。ない場合はエラー処理3-1
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店定義ファイルを開く。開けなかった場合、エラー処理1-1
		if(!fileRead(args[0] + File.separator + "branch.lst","支店定義ファイル","^[0-9]{3}",branchNames,branchSales)) {
			return;
		}

		//商品定義ファイルを開く。開けなかった場合、エラー処理2-1
				if(!fileRead(args[0] + File.separator + "commodity.lst","商品定義ファイル","^[A-Za-z0-9]{8}",commodityName,commoditySales)) {
					return;
				}

		//２．集計
		//対象ディレクトリのファイル名を取得
		File sale = new File(args[0]);
		File[] sales = sale.listFiles();

		//売上ファイルのリスト
		List<File> salesFile = new ArrayList<>();

		//拡張子rcd且つファイル名が数字８桁のファイルを検索し、売上ファイルのリストに格納
		for(int i = 0; i<sales.length; i++) {
			if(sales[i].getName().matches("^[0-9]{8}.+rcd$")) {
				salesFile.add(sales[i]);
			}
		}

		Collections.sort(salesFile);

		//ファイル名連番になっているか確認処理。エラー処理3-1
		for(int i = 0; i<salesFile.size()-1;i++) {
			//ファイル名連番になっているか確認用変数
			int former = Integer.parseInt(salesFile.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(salesFile.get(i+1).getName().substring(0,8));
			if(former +1 != latter){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		//売上ファイルを読み込み集計をおこなう。
		for(int i = 0; i<salesFile.size();i++) {
			try {
				//該当ファイルがある場合、ファイルを読み込み売上金額累計用マップに格納していく。
				FileReader salsefr = new FileReader(salesFile.get(i));
				br = new BufferedReader(salsefr);

				//支店コードと金額を取得する。3行ではなかった場合はエラー処理3-5。また、売上金額が数字出なかった場合エラー処理4-2
				String line = null;
				List<String> saleInfo = new ArrayList<String>();
				while((line = br.readLine())!= null) {
					saleInfo.add(line);
				}
				if(saleInfo.size() != 3) {
					System.out.println(salesFile.get(i).getName()+"のフォーマットが不正です");
					return;
				}

				//支店コードと商品コード、金額を格納する。売上金額が数字ではなかった場合エラー処理4-2
				if(!saleInfo.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店別売上金額累計用マップに処理対象支店コードがあるか確認。ない場合エラー処理3-3
				if(!branchSales.containsKey(saleInfo.get(0))) {
					System.out.println(salesFile.get(i).getName()+"の支店コードが不正です");
					return;
				}

				//商品別金額累計用マップに処理対象商品コードがあるか確認。ない場合エラー処理3-4
				if(!commoditySales.containsKey(saleInfo.get(1))) {
					System.out.println(salesFile.get(i).getName()+"の商品コードが不正です");
					return;
				}

				long branchValue = branchSales.get(saleInfo.get(0))+Long.parseLong(saleInfo.get(2));
				long commodityValue = commoditySales.get(saleInfo.get(1))+Long.parseLong(saleInfo.get(2));
				if(branchValue >= 10000000000L && commodityValue >= 10000000000L) {
					//10桁超えたエラーが出てくる。エラー処理3-2
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(saleInfo.get(0), branchValue);
				commoditySales.put(saleInfo.get(1), commodityValue);
			}catch(Exception e) {
				//エラー3-3
				System.out.println("予期せぬエラーが発生しました");
			}finally {
				if(br!=null) {
					try {
						br.close();
					}catch(IOException ie) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}
		//３．ファイル出力
		fileOutput(args[0]+ File.separator + "branch.out",branchNames,branchSales);
		fileOutput(args[0]+ File.separator + "commodity.out",commodityName,commoditySales);
	}

	/**
	 * ファイル読込メソッド
	 * @param filePath 読み込むファイルのパス
	 * @param fileName 読み込むファイル名
	 * @param fileType ファイルの種別
	 * @param format ファイルフォーマット
	 * @param definitionMap 定義ファイルマップ
	 * @param salesMap 売上ファイルマップ
	 */
	public static boolean fileRead(String filePath, String fileName, String regex,Map<String,String> names,Map<String,Long> sales) {
		BufferedReader br = null;
		File file = new File(filePath);
		if(!file.exists()) {
			System.out.println(fileName+"が存在しません");
			return false;
		}
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//定義ファイルを1行ずつ読み込み、すべてのコードとそれに対応する値を保持
			String line = null;

			while((line = br.readLine())!= null) {
				//ファイルフォーマットの中身を確認。違った場合エラー処理1-2もしくは2-2
				String[] data = line.split(",");
				if(!data[0].matches(regex)) {
					System.out.println(fileName+"のフォーマットが不正です");
					br.close();
					return false;
				}
				names.put(data[0], data[1]);
				sales.put(data[0], (long) 0);
			}
		}catch(Exception e) {
			//エラー3-3
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br!=null) {
				try {
					br.close();
				}catch(IOException ie) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * ファイル書込メソッド
	 * @param filePath 書き込むファイルのパス
	 * @param fileName 書き込むファイル名
	 * @param definitionMap 定義ファイルマップ
	 * @param salesMap 売上ファイルマップ
	 */
	public static boolean fileOutput(String filePath,Map<String,String> names,Map<String,Long> sales) {
		PrintWriter pw = null;
		try{
			//集計結果出力。コマンドライン引数のディレクトリに集計ファイルを作成
			File out = new File(filePath);
			FileWriter outFile = new FileWriter(out);
			pw = new PrintWriter(new BufferedWriter(outFile));
			for(Map.Entry<String,String> outNames : names.entrySet()) {
				pw.println(outNames.getKey() + "," +outNames.getValue() + "," + sales.get(outNames.getKey()));
			}
		}catch(IOException e) {
			//エラー4-3
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(pw != null) {
				pw.close();
			}
		}
		return true;
	}

}
